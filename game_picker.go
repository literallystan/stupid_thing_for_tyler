package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

func main() {
	var dir string
	for {
		fmt.Println("Enter a directory to randomize or q to quit")
		fmt.Scanln(&dir)
		if dir == "" {
			dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println("No directory entered, using: " + dir)
		} else if dir == "q" {
			break
		} else {
			os.Chdir(dir)
		}

		files, err := filepath.Glob("*")
		if err != nil {
			log.Fatal(err)
		}
		rand.Seed(time.Now().UnixNano()) //generate seeed based on the time to prevent repeat results
		game := files[rand.Intn(len(files))]
		fmt.Println(game)
		os.Chdir(dir + "\\" + game)
		gamePath := dir + "\\" + game + "\\" + game + ".exe"
		_, err = exec.LookPath(gamePath)
		if err == nil {
			fmt.Println("Game Found: Running " + game + ".exe")
			cmd := exec.Command(gamePath)
			cmd.Start()
		} else { //check for a lower case whitespace stipped version of the exe and repeat the process
			reg, err := regexp.Compile("[^a-zA-Z0-9]+")
			if err != nil {
				log.Fatal(err)
			}
			strippedGame := reg.ReplaceAllString(game, "")
			loweredGamePath := dir + "\\" + game + "\\" + strings.ToLower(strippedGame) + ".exe"
			_, err = exec.LookPath(loweredGamePath)
			if err == nil {
				fmt.Println("Game Found: Running " + game + ".exe")
				cmd := exec.Command(loweredGamePath)
				cmd.Start()
			} else {
				fmt.Printf("exe for %s not found, folder is probably named differently than the game. \n", game)
			}
		}
	}
}
